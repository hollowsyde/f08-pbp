from django.urls import path
from .views import rs_info, edit_profile

urlpatterns = [
    path('', rs_info, name='info'),
    path('edit-profile/', edit_profile, name='edit_profile'),
    # path('edit-profile/<str:pk>', edit_profile, name='edit_profile')
    # path('edit/', '', name='edit'),
]