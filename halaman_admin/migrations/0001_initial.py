# Generated by Django 3.2.7 on 2021-10-21 11:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='RumahSakit',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=20)),
                ('lokasi', models.CharField(max_length=30)),
                ('nomor_telepon', models.CharField(max_length=20)),
                ('vaksin_tersedia', models.PositiveIntegerField(max_length=1000000000)),
                ('bed_tersedia', models.PositiveIntegerField(max_length=200)),
            ],
        ),
    ]
