# Generated by Django 3.2.7 on 2021-10-24 07:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('halaman_admin', '0002_auto_20211021_1845'),
    ]

    operations = [
        migrations.AddField(
            model_name='rumahsakit',
            name='bed_ada',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='rumahsakit',
            name='vaksin_ada',
            field=models.BooleanField(default=False),
        ),
    ]
