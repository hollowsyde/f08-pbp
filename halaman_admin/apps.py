from django.apps import AppConfig


class HalamanAdminConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'halaman_admin'
