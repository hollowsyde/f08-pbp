from django import forms
from .models import Artikel

class ArtikelForm(forms.ModelForm):
    class Meta:
        model = Artikel
        fields = [
            'penulis',
            'asal_rumah_sakit',
            'judul',
            'isi'
        ]