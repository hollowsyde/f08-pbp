from django.shortcuts import render
from halaman_artikel.models import Artikel
from .forms import ArtikelForm
from django.http import response, HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from rest_framework import viewsets
from halaman_artikel.serializers import ArtikelSerializer


def daftar_artikel(request):
    artikels = Artikel.objects.all()
    response = {'artikels' : artikels}
    return render(request, 'daftar_artikel.html', response)
    
# Create your views here.
@login_required(login_url='login')
def tambah_artikel(request):

    if request.method == "POST":

        form = ArtikelForm(request.POST or None);

        response_data = {}

        if form.is_valid():
            form.save()
            response_data['hasil'] = 'Artikel berhasil ditambahkan'
            response_data['berhasil'] = True
            return JsonResponse(response_data)

        else:
            response_data['hasil'] = 'Ada kesalahan dalam memasukkan data. Cek kembali.'
            response_data['berhasil'] = False
            return JsonResponse(response_data)

        
    context = {}
    return render(request,"tambah_artikel.html",context)

class ArtikelViewSet(viewsets.ModelViewSet):
    queryset = Artikel.objects.all()
    serializer_class = ArtikelSerializer