from rest_framework import serializers

from halaman_artikel.models import Artikel

class ArtikelSerializer(serializers.ModelSerializer):
   class Meta:
       model = Artikel
       fields = "__all__"