from django.apps import AppConfig


class LogregpageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'logregpage'
