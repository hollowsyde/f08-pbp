from django.urls import path
from .views import regHosp, logInHosp, logOutHosp, login_flutter, logout_flutter, reg_flutter

from django.conf.urls import url

urlpatterns = [
    path('register', regHosp, name='register'),
    path('login', logInHosp, name='login'),
    path('logout', logOutHosp, name='logout'),
    path('loginf', login_flutter, name='loginf'),
    path('logoutf', logout_flutter, name='logoutf'),
    path('regf', reg_flutter, name='regf')
]