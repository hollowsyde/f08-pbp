from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt

from .decorators import unauthenticated_user
from .forms import CreateDefUserForm, CreateHospForm
from .models import RumahSakit

import json

# Create your views here.

@unauthenticated_user
def regHosp(request):

    if request.method == 'POST':

        dataUser = {
            'username':request.POST.get('username'),
            'email':request.POST.get('email'),
            'password1':request.POST.get('password1'),
            'password2':request.POST.get('password2')
        }

        dataHosp = {
            'nama':request.POST.get('nama'),
            'lokasi':request.POST.get('lokasi'),
            'nomor_telepon':request.POST.get('nomor_telepon'),
            'vaksin_ada':request.POST.get('vaksin_ada'),
            'vaksin_tersedia':request.POST.get('vaksin_tersedia'),
            'bed_ada':request.POST.get('bed_ada'),
            'bed_tersedia':request.POST.get('bed_tersedia')
        }

        form = CreateDefUserForm(dataUser or None)
        hosp_form = CreateHospForm(dataHosp or None)

        response_data = {}

        if form.is_valid() and hosp_form.is_valid():
            user = form.save()

            hosp = hosp_form.save(commit=False)
            hosp.user = user

            hosp.save()

            response_data['result'] = 'Register Rumah Sakit Berhasil.'

            return JsonResponse(response_data)
        
        else:
            response_data['result'] = 'Terdapat bagian form yang tidak valid. Mohon untuk melakukan pengecekan kembali.'

            return JsonResponse(response_data)

            
    context = {}
    return render(request, 'register1.html', context)

@unauthenticated_user
def logInHosp(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_superuser:
                login(request, user)
                return redirect('info')
            else:
                rs_target = find_rs(user)
                
                if rs_target.rs_confirmed:
                    login(request, user)
                    return redirect('info')
                elif not rs_target.rs_confirmed:
                    messages.warning(request, "Rumah Sakit belum terkonfirmasi. Mohon menunggu konfirmasi kami...")
                    
        else:
            messages.warning(request, "Username atau Password salah")
    
    context = {}
    return render(request, 'login.html', context)

@csrf_exempt
def login_flutter(request):

    data = json.loads(request.body)
    username = data["username"]
    password = data["password"]

    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_superuser:
            login(request, user)
            return JsonResponse({"status" : "admin", "username" : username})
        else:
            rs_target = find_rs(user)
            
            if rs_target.rs_confirmed:
                login(request, user)
                return JsonResponse({"status" : "adminrs", "username" : username})
            elif not rs_target.rs_confirmed:
                return JsonResponse({"status" : "not confirmed"})
    else:
        return JsonResponse({"status" : "not registered"})

def logOutHosp(request):
    logout(request)
    return redirect('login')

def find_rs(user):

    rs = RumahSakit.objects.all()

    for rumah_sakit in rs:
        if rumah_sakit.user == user:
            return rumah_sakit
    
    return None