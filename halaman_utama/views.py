import json
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render
from halaman_admin.models import RumahSakit
from django.shortcuts import render, redirect
from .forms import FormMasukan,FormMasukanAbel,FormMasukanAgnes
from .models import Masukan
from django.shortcuts import get_object_or_404
from rest_framework import generics, viewsets
from django.core import serializers

from halaman_utama.serializers import DetailRumahSakitSerializer, MasukanSerializer, RumahSakitSerializer
sign = "Info RS"
# Create your views here.

def index(request): # AVEL : baru : buat form masukann
    form = FormMasukanAbel()

    #context= {'form':form}
    response ={'sign':sign,'form':form}
    return render(request,'home.html',response)
    
# Create your views here.
def list_rumah_sakit_bed(request):
    rumah_sakit = RumahSakit.objects.filter(bed_ada = True)
    rs = RumahSakit.objects.all()

    form = FormMasukanAbel()

    for calon in rs:
        if calon.user == request.user:
            rs_target = ", "+ calon.nama
            break
        else:
            rs_target = ""

    context= {'form':form,'rumahsakits':rumah_sakit,'user':rs_target}
    return render(request, 'katalog_bed.html',context)

def list_rumah_sakit_vaksin(request):
    rumah_sakit = RumahSakit.objects.filter(vaksin_ada = True)
    rs = RumahSakit.objects.all()

    form = FormMasukanAbel()
    for calon in rs:
        if calon.user == request.user:
            rs_target = ", "+ calon.nama
            break
        else:
            rs_target = ""

    context= {'form':form,'rumahsakits':rumah_sakit,'user':rs_target}
    return render(request, 'katalog_vaksin.html',context)
    
def add_form(request):
    context={}
    form = FormMasukan(request.POST)

    if form.is_valid():
       form.save()
       return redirect("")

    else:
        form = FormMasukan() 

    context['form'] = form
    return render(request, "", context)

def masukan_list(request):
    masukan = Masukan.objects.all()
    response = {'masukan' : masukan}
    return render(request, '', response)

def show_biodata(request, id):
    response = get_object_or_404(RumahSakit, pk=id)
    form = FormMasukanAgnes(request.POST or None)
    rs = RumahSakit.objects.all()
    for calon in rs:
        if calon.user == request.user:
            rs_target = calon.nama
            break
        else:
            rs_target = "Pengunjung"
    if(form.is_valid() and request.method == 'POST'):
        form.save()
    
    context= {'rs':response, 'form':form, 'user_type':rs_target}
    return render(request, 'biodata.html', context)
      
def echa(request):
    form = FormMasukanAbel()
    if request.is_ajax():
        form = FormMasukanAbel(request.POST)
        print(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse({
                'message': 'success'
            })
        else:
            return JsonResponse({
                'message': 'gagal'
            })

    return render(request, 'katalog_vaksin.html', {'form': form})


###################################################################################################################

class RumahSakitViewSet(viewsets.ModelViewSet):
    queryset = RumahSakit.objects.all()
    serializer_class = RumahSakitSerializer

def masukanmobile(request):
    
    if request.method == 'POST':

        masukan = {
            'nama':request.POST.get('username'),
            'email':request.POST.get('email'),
            'isi':request.POST.get('isi'),
            
        }
        form = FormMasukanAbel(masukan or None)
        if form.is_valid() :
            form.save()          
  

###################################################################################################################

class DetailRumahSakitViewSet(viewsets.ModelViewSet):
    queryset = RumahSakit.objects.all()
    serializer_class = DetailRumahSakitSerializer

class MasukanViewSet(viewsets.ModelViewSet):
    queryset = Masukan.objects.all()
    serializer_class = MasukanSerializer
