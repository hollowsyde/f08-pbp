from django.conf.urls import include
from django.urls import path
from .views import DetailRumahSakitViewSet, MasukanViewSet, index, list_rumah_sakit_bed, list_rumah_sakit_vaksin, show_biodata, echa,RumahSakitViewSet,masukanmobile

from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'rumahsakit', RumahSakitViewSet)
router.register(r'detail', DetailRumahSakitViewSet)
router.register(r'masukan', MasukanViewSet)
urlpatterns = [
    path('', index, name='index'),
    path('vaksin-rumahsakit',list_rumah_sakit_vaksin),
    path('bed-rumahsakit',list_rumah_sakit_bed,name='bed-rumahsakit'),
    path('katalog-masukan',echa,name="katalog-masukan"),
    path('bed-rumahsakit/<int:id>/', show_biodata, name='show_bio'),
    path('vaksin-rumahsakit/<int:id>/', show_biodata, name='show_bio'),
    path('', include(router.urls)),
    path('masukanmobile',masukanmobile,name='masukanmobile'),
]
