from rest_framework import serializers

from halaman_admin.models import RumahSakit
from halaman_utama.models import Masukan

class RumahSakitSerializer(serializers.ModelSerializer):
   class Meta:
       model = RumahSakit
       fields = "__all__"


class DetailRumahSakitSerializer(serializers.ModelSerializer):
   class Meta:
       model = RumahSakit
       fields = ['nama','lokasi','nomor_telepon','vaksin_tersedia','bed_tersedia']


class MasukanSerializer(serializers.ModelSerializer):
   class Meta:
       model = Masukan
       fields = ['nama','email','isi']

