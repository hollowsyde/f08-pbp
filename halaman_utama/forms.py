from django.db.models import fields
from django.forms import widgets
from halaman_utama.models import Masukan
from django import forms

class FormMasukan(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['nama'].widget.attrs.update({
            'required':'',
            'name':'nama',
            'id':'To',
            'type':'text',
            'class':'form-input',
            'placeholder':'Silakan isi nama Anda',
            'maxlength':'16',
            'minlength':'1'
        })
        self.fields['email'].widget.attrs.update({
            'required':'',
            'name':'email',
            'id':'From',
            'type':'text',
            'class':'form-input',
            'placeholder':'Silakan isi email',
            'maxlength':'16',
            'minlength':'1'
        })
        self.fields['isi'].widget.attrs.update({
            'required':'',
            'name':'isi',
            'id':'Title',
            'type':'text',
            'class':'form-input',
            'placeholder':'Silakan isi masukan Anda',
            'maxlength':'16',
            'minlength':'1'
        })

    class Meta:
        model = Masukan
        fields = "__all__"
        widgets={
            'nama' : forms.TextInput(attrs={'class':'form-control'}),
            'email' : forms.TextInput(attrs={'class':'form-control'}),
            'isi' : forms.Textarea(attrs={'class':'form-control'})
        }
class FormMasukanAbel(forms.ModelForm):
    class Meta:
        model = Masukan
        fields = "__all__"

class FormMasukanAgnes(forms.ModelForm):
    class Meta:
        model = Masukan
        fields = "__all__"
